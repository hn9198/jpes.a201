/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArithmeticOperators_2;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class ArithmeticOperators2 {

    public static void main(String[] args) {
        double r, h, sA, bA, v;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input radius: ");
        r = sc.nextDouble();
        System.out.print("Input height: ");
        h = sc.nextDouble();
        sA = 2 * Math.PI * r * h;
        bA = 2 * Math.PI * r * r + sA;
        v = Math.PI * r * r * h;
        System.out.print("Surface area = ");
        System.out.format("%.10f", sA);
        System.out.print("\nSurface area = ");
        System.out.format("%.10f", bA);
        System.out.print("\nVolume = ");
        System.out.format("%.10f", v);
        System.out.println("\n");
    }
}
