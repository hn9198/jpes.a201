/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatatypeandOperators;

/**
 *
 * @author HienDepTrai
 */
public class Main {

    public static void main(String[] args) {
        int o1 = (101 + 0) / 3;
        double o2 = 3.0e-6 * 10000000.1;
        boolean o3 = true && true;
        boolean o4 = false && true;
        boolean o5 = (false && false) || (true && true);
        boolean o6 = (false || false) && (true && true);

        System.out.println("(101 + 0) / 3)-> " + o1);
        System.out.println("(3.0e-6 * 10000000.1)-> " + o2);
        System.out.println("(true && true)-> " + o3);
        System.out.println("(false && true)-> " + o4);
        System.out.println("((false && false) || (true && true))-> " + o5);
        System.out.println("(false || false) && (true && true)-> " + o6);
    }
}
