/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LogicalOperators;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class LogicalOperators {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input first number:");
        int n1 = sc.nextInt();
        System.out.print("Input second number:");
        int n2 = sc.nextInt();
        System.out.print("Input third number:");
        int n3 = sc.nextInt();
        System.out.print("Input fourth number:");
        int n4 = sc.nextInt();
        if (n1 == n2 && n2 == n3 && n3 == n4) {
            System.out.println("Numbers are equal!");
        } else {
            System.out.println("Numbers are not equal!");
        }
    }
}
